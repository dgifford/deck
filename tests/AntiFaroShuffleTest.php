<?php

Namespace dgifford\Deck\Tests;


use dgifford\Deck\Deck;


/**
 * Auto Loader
 *
 */
require_once(__DIR__ . '/../vendor/autoload.php');


class AntiFaroShuffleTest extends \PHPUnit\Framework\TestCase
{
    public function testAntiOutFaroWithLimitHalfEvenTotal()
    {
        $deck = Deck::make(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c',]);

        // Out - top and bottom cards stay on outside
        $deck->shuffleAntiFaro(4, false);

        $this->assertSame(
            ['ac', '3c', '5c', '7c', '2c', '4c', '6c', '8c',]
            , $deck->asLetters()
        );
    }


    public function testAntiInFaroWithLimitHalfEvenTotal()
    {
        $deck = Deck::make(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c',]);

        // In - top and bottom cards go into deck
        $deck->shuffleAntiFaro( 4);

        $this->assertSame(
            ['2c', '4c', '6c', '8c', 'ac', '3c', '5c', '7c', ]
            , $deck->asLetters()
        );
    }


    public function testAntiOutFaroWithLimitLessThanHalfEvenTotal()
    {
        $deck = Deck::make(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c',]);

        $deck->shuffleAntiFaro( 3, false);

        $this->assertSame(
            ['ac', '3c', '5c', '2c', '4c', '6c', '7c', '8c', ]
            , $deck->asLetters()
        );
    }


    public function testAntiInFaroReversesInFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleInFaro();

        $deck->shuffleInAntiFaro();

        $this->assertSame(
            Deck::make()->setClubs()->asLetters()
            , $deck->asLetters()
        );
    }



    public function testAntiOutFaroReversesOutFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleOutFaro();

        $deck->shuffleOutAntiFaro();

        $this->assertSame(
            Deck::make()->setClubs()->asLetters()
            , $deck->asLetters()
        );
    }


    public function testLimitedAntiInFaroReversesInFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro( 4, true);

        $deck->shuffleAntiFaro( 4, true);

        $this->assertSame(
            Deck::make()->setClubs()->asLetters()
            , $deck->asLetters()
        );
    }


    public function testLimitedAntiOutFaroReversesOutFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro( 3, false);

        $deck->shuffleAntiFaro( 3, false);

        $this->assertSame(
            Deck::make()->setClubs()->asLetters()
            , $deck->asLetters()
        );
    }



    public function testLimitMoreThanHalfTreatedAsHalf()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleAntiFaro( 10, true);

        $this->assertSame(
            $deck->asLetters()
            , ['2c', '4c', '6c', '8c', '10c', 'qc', 'ac', '3c', '5c', '7c', '9c', 'jc', 'kc',]
        );

        $deck = Deck::make()->setClubs();

        $deck->shuffleAntiFaro( 10, false);

        $this->assertSame(
            $deck->asLetters()
            , ['ac', '3c', '5c', '7c', '9c', 'jc', 'kc','2c', '4c', '6c', '8c', '10c', 'qc', ]
        );

        $deck = Deck::make()->setClubs();

        $deck->shuffleAntiFaro( 20, false);

        $this->assertSame(
            $deck->asLetters()
            , ['ac', '3c', '5c', '7c', '9c', 'jc', 'kc','2c', '4c', '6c', '8c', '10c', 'qc', ]
        );
    }




    public function testShuffleOutAntiFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleOutAntiFaro();

        $this->assertSame(
            $deck->asLetters()
            , ['ac', '3c', '5c', '7c', '9c', 'jc', '2c', '4c', '6c', '8c', '10c', 'qc', 'kc',]
        );
    }




    public function testShuffleInAntiFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleInAntiFaro();

        $this->assertSame(
            $deck->asLetters()
            , ['2c', '4c', '6c', '8c', '10c', 'qc', 'ac', '3c', '5c', '7c', '9c', 'jc', 'kc',]
        );
    }


}