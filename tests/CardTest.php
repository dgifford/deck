<?php

Namespace dgifford\Deck\Tests;


use dgifford\Deck\Card;


/**
 * Auto Loader
 *
 */
require_once(__DIR__ . '/../vendor/autoload.php');


class CardTest extends \PHPUnit\Framework\TestCase
{
    public function testInstantiateCardWithFrontOnly()
    {
        $card = new Card('as');

        $this->assertTrue($card instanceof Card);

        $this->assertSame('as', $card->getFront());

        $this->assertSame('bb', $card->getBack());

        $this->assertSame('spade', $card->getType());

        $this->assertSame('s', $card->getSuitLetter());

        $this->assertSame('a', $card->getValue());

        $this->assertTrue($card->hasSuit());
    }


    public function testInstantiateCardWithFrontAndBackSeperate()
    {
        $card = new Card('as', 'rb');

        $this->assertTrue($card instanceof Card);

        $this->assertSame('as', $card->getFront());

        $this->assertSame('rb', $card->getBack());

        $this->assertSame('spade', $card->getType());

        $this->assertSame('s', $card->getSuitLetter());

        $this->assertSame('a', $card->getValue());
    }


    public function testInstantiateCardWithFrontAndBackCombined()
    {
        $card = new Card('as:rb');

        $this->assertTrue($card instanceof Card);

        $this->assertSame('as', $card->getFront());

        $this->assertSame('rb', $card->getBack());

        $this->assertSame('spade', $card->getType());

        $this->assertSame('s', $card->getSuitLetter());

        $this->assertSame('a', $card->getValue());
    }


    public function testInstantiateCardWithEmptyOptions()
    {
        $card = new Card('as:rb:');

        $this->assertTrue($card instanceof Card);

        $this->assertSame('as', $card->getFront());

        $this->assertFalse($card->getSelected());

        $this->assertFalse($card->getRevolved());

        $this->assertFalse($card->getReversed());
    }


    public function testInstantiateWithStringOptions()
    {
        $card = new Card('as:rb:s');

        $this->assertSame('as', $card->getFront());

        $this->assertTrue($card->getSelected());

        $this->assertFalse($card->getRevolved());

        $this->assertFalse($card->getReversed());

        $card = new Card('as:rb:sor');

        $this->assertSame('as', $card->getFront());

        $this->assertTrue($card->getSelected());

        $this->assertTrue($card->getRevolved());

        $this->assertTrue($card->getReversed());
    }


    public function testInstantiateCardWithArray()
    {
        $card = new Card(['front' => 'as', 'back' => 'rb', 'reversed' => true]);

        $this->assertTrue($card instanceof Card);

        $this->assertSame('as', $card->getFront());

        $this->assertSame('rb', $card->getBack());

        $this->assertSame('spade', $card->getType());

        $this->assertSame('s', $card->getSuitLetter());

        $this->assertSame('a', $card->getValue());

        $this->assertTrue($card->getReversed());
    }


    public function testInstantiateCardWithArrayContainingInvalidValues()
    {
        $card = new Card(['foo' => 'bar', 'front' => 'as', 'back' => 'rb', 'reversed' => true]);

        $this->assertTrue($card instanceof Card);

        $this->assertSame('as', $card->getFront());

        $this->assertSame('rb', $card->getBack());

        $this->assertSame('spade', $card->getType());

        $this->assertSame('s', $card->getSuitLetter());

        $this->assertSame('a', $card->getValue());

        $this->assertTrue($card->getReversed());
    }


    public function testInstantiateJoker()
    {
        $card = new Card('j1', 'rb');

        $this->assertTrue($card instanceof Card);

        $this->assertSame('j1', $card->getFront());

        $this->assertSame('rb', $card->getBack());

        $this->assertSame('joker', $card->getType());

        $this->assertSame('', $card->getSuitLetter());

        $this->assertSame('', $card->getValue());

        $this->assertFalse($card->hasSuit());
    }


    public function testInstantiateUnknowCard()
    {
        $card = new Card('xx');

        $this->assertTrue($card instanceof Card);

        $this->assertSame('xx', $card->getFront());

        $this->assertSame('bb', $card->getBack());

        $this->assertSame('unknown', $card->getType());

        $this->assertSame('', $card->getSuitLetter());

        $this->assertSame('', $card->getValue());

        $this->assertFalse($card->hasSuit());
    }


    public function testInstantiateCustomCard()
    {
        $card = new Card('foo', 'rb');

        $this->assertTrue($card instanceof Card);

        $this->assertSame('foo', $card->getFront());

        $this->assertSame('rb', $card->getBack());

        $this->assertFalse($card->hasSuit());
    }


    public function testInvalidEmptyString()
    {
        $this->expectException(\InvalidArgumentException::class);

        $card = new Card('');
    }


    public function testInvalidStringWithSpace()
    {
        $this->expectException(\InvalidArgumentException::class);

        $card = new Card(' ');
    }


    public function testInvalidEmptyArray()
    {
        $this->expectException(\InvalidArgumentException::class);

        $card = new Card([]);
    }


    public function testInvalidFrontType()
    {
        $this->expectException(\InvalidArgumentException::class);

        $card = new Card(' ');
    }


    public function testSetRevolved()
    {
        $card = new Card('as');

        $this->assertFalse($card->getRevolved());

        $card->setRevolved(true);

        $this->assertTrue($card->getRevolved());
    }


    public function testToggleRevolved()
    {
        $card = new Card('2c');

        $this->assertFalse($card->getRevolved());

        $card->toggleRevolved();

        $this->assertTrue($card->getRevolved());

        $card->toggleRevolved();

        $this->assertFalse($card->getRevolved());
    }


    public function testSetReversed()
    {
        $card = new Card('as');

        $this->assertFalse($card->getReversed());

        $card->setReversed(true);

        $this->assertTrue($card->getReversed());
    }


    public function testToggleReversed()
    {
        $card = new Card('2c');

        $this->assertFalse($card->getReversed());

        $card->toggleReversed();

        $this->assertTrue($card->getReversed());

        $card->toggleReversed();

        $this->assertFalse($card->getReversed());
    }


    public function testSetType()
    {
        $card = new Card('as');

        $this->assertSame('spade', $card->getType());
    }


    public function testGetClubSuitInfo()
    {
        $card = new Card('2c');

        $this->assertSame('c', $card->getSuitLetter());
    }


    public function testGetJoker()
    {
        $card = new Card('j1');

        $this->assertSame('', $card->getSuitLetter());

        $this->assertSame('j1', $card->front());

        $this->assertSame('bb', $card->back());
    }


    public function testGetTwoOfClubs()
    {
        $card = new Card('2c');

        $this->assertSame('2c', $card->front());

        $this->assertSame('bb', $card->back());
    }


    public function testToggleSelected()
    {
        $card = new Card('2c');

        $this->assertFalse($card->getSelected());

        $card->toggleSelected();

        $this->assertTrue($card->getSelected());

        $card->toggleSelected();

        $this->assertFalse($card->getSelected());
    }


    public function testSetSelected()
    {
        $card = new Card('2c');

        $card->setSelected(false);

        $this->assertFalse($card->getSelected());

        $card->setSelected(true);

        $this->assertTrue($card->getSelected());
    }


    public function testGetID()
    {
        $card = new Card('ac');

        $this->assertNull($card->getID());

        $card->setID(0);

        $this->assertSame(0, $card->getID());
    }


    public function testGetCode()
    {
        $card = new Card('ah');

        $this->assertSame('ah:bb', $card->getCode());

        $card->setSelected(true);

        $this->assertSame('ah:bb:s', $card->getCode());

        $card->setRevolved(true);

        $this->assertSame('ah:bb:so', $card->getCode());

        $card->setReversed(true);

        $this->assertSame('ah:bb:sro', $card->getCode());

        $card->setSelected(false);

        $this->assertSame('ah:bb:ro', $card->getCode());
    }
}