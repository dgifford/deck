<?php
Namespace dgifford\Deck\Tests;


use dgifford\Deck\Card;
use dgifford\Deck\Deck;


/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class DeckSetOrderTest extends \PHPUnit\Framework\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        
        $this->deck = Deck::make();
    }
    
    
    
	public function testSetClubs()
	{
        $this->deck->setClubs();

		$this->assertFalse( $this->deck->has52StandardCards() );

		$this->assertSame(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $this->deck->asLetters() );
	}
    
    
    
	public function testSetHearts()
	{
        $this->deck->setHearts();

		$this->assertFalse( $this->deck->has52StandardCards() );

		$this->assertSame(['ah', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', 'jh', 'qh', 'kh',], $this->deck->asLetters() );
	}
    
    
    
	public function testSetSpades()
	{
        $this->deck->setSpades();

		$this->assertFalse( $this->deck->has52StandardCards() );

		$this->assertSame(['as', '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', '10s', 'js', 'qs', 'ks',], $this->deck->asLetters() );
	}
    
    
    
	public function testSetDiamonds()
	{
        $this->deck->setDiamonds();

		$this->assertFalse( $this->deck->has52StandardCards() );

		$this->assertSame(['ad', '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', '10d', 'jd', 'qd', 'kd',], $this->deck->asLetters() );
	}



	public function testSetEmpty()
	{
        $this->deck->setDiamonds();

        $this->deck->set();

		$this->assertSame([], $this->deck->asLetters() );
	}


    
    public function testSetBicycle()
    {
        $this->deck->setBicycle();

        $this->assertTrue( $this->deck->has52StandardCards() );

        $this->assertSame(
            [
                'p1', 'p2',
                'ah', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', 'jh', 'qh', 'kh',
                'ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
                'kd', 'qd', 'jd', '10d', '9d', '8d', '7d', '6d', '5d', '4d', '3d', '2d', 'ad',
                'ks', 'qs', 'js', '10s', '9s', '8s', '7s', '6s', '5s', '4s', '3s', '2s', 'as',
                'j1', 'j2',
            ], $this->deck->asLetters() );

        $this->assertSame( 'promotional', $this->deck[0]->getType() );

        $this->assertSame( 'heart', $this->deck[2]->getType() );

        $this->assertSame( 'joker', $this->deck[54]->getType() );
    }



    public function testSetCopag()
    {
        $this->deck->setCopag();

        $this->assertTrue( $this->deck->has52StandardCards() );

        $this->assertSame(
        [
            0 => 'bb:bb:',
            1 => 'bl:bb:',
            2 => 'j1:bb:',
            3 => 'ah:bb:',
            4 => '2h:bb:',
            5 => '3h:bb:',
            6 => '4h:bb:',
            7 => '5h:bb:',
            8 => '6h:bb:',
            9 => '7h:bb:',
            10 => '8h:bb:',
            11 => '9h:bb:',
            12 => '10h:bb:',
            13 => 'jh:bb:',
            14 => 'qh:bb:',
            15 => 'kh:bb:',
            16 => 'ac:bb:',
            17 => '2c:bb:',
            18 => '3c:bb:',
            19 => '4c:bb:',
            20 => '5c:bb:',
            21 => '6c:bb:',
            22 => '7c:bb:',
            23 => '8c:bb:',
            24 => '9c:bb:',
            25 => '10c:bb:',
            26 => 'jc:bb:',
            27 => 'qc:bb:',
            28 => 'kc:bb:',
            29 => 'kd:bb:',
            30 => 'qd:bb:',
            31 => 'jd:bb:',
            32 => '10d:bb:',
            33 => '9d:bb:',
            34 => '8d:bb:',
            35 => '7d:bb:',
            36 => '6d:bb:',
            37 => '5d:bb:',
            38 => '4d:bb:',
            39 => '3d:bb:',
            40 => '2d:bb:',
            41 => 'ad:bb:',
            42 => 'ks:bb:',
            43 => 'qs:bb:',
            44 => 'js:bb:',
            45 => '10s:bb:',
            46 => '9s:bb:',
            47 => '8s:bb:',
            48 => '7s:bb:',
            49 => '6s:bb:',
            50 => '5s:bb:',
            51 => '4s:bb:',
            52 => '3s:bb:',
            53 => '2s:bb:',
            54 => 'as:bb:',
            55 => 'j2:bb:',
        ], $this->deck->asCodes() );
    }



    public function testSetSuited()
    {
        $this->deck->setSuited();

        $this->assertTrue( $this->deck->has52StandardCards() );

        $this->assertSame(
            [
                'ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
                'ah', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', 'jh', 'qh', 'kh',
                'as', '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', '10s', 'js', 'qs', 'ks',
                'ad', '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', '10d', 'jd', 'qd', 'kd',
            ], $this->deck->asLetters() );
    }



    public function testSetAlphabet()
    {
        $this->deck->setAlphabet();

        $this->assertSame(
            [
                'a', 'a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'e', 'f',
                'f', 'g', 'g', 'h', 'h', 'i', 'i', 'j', 'j', 'k', 'k', 'l', 'l',
                'm', 'm', 'n', 'n', 'o', 'o', 'p', 'p', 'q', 'r', 'r', 's', 's',
                't', 't', 't', 'u', 'u', 'v', 'v', 'w', 'w', 'x', 'y', 'y', 'z',
            ], $this->deck->asLetters() );
    }


    public function testSetRandom()
    {
        $this->deck->setRandom();

        $this->assertTrue( $this->deck->has52StandardCards() );

        $this->assertSame( 52, $this->deck->count() );
    }



    public function testSetAll()
    {
        $this->deck->setAll();

        $this->assertTrue( $this->deck->has52StandardCards() );

        $this->assertSame( Deck::standardCardsCount(), $this->deck->count() );
    }




}