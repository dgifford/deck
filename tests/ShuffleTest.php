<?php
Namespace dgifford\Deck\Tests;



use dgifford\Deck\Deck;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class ShuffleTest extends \PHPUnit\Framework\TestCase
{
	public function testInstantiateDeck()
	{
		$deck = Deck::make()->setBicycle();

		$this->assertTrue( $deck->has52StandardCards() );
	}



	public function testRiffleShuffle()
	{
		$deck = Deck::make()->setBicycle();

		$before = $deck->asLetters();


		$deck->shuffleRiffle();

		$this->assertTrue( $deck->has52StandardCards() );

		$this->assertFalse( $before === $deck->asLetters() );
	}



	public function testOverhandShuffle()
	{
		$deck = Deck::make()->setBicycle();

		$before = $deck->asLetters();

		$deck->shuffleOverhand();

		$this->assertTrue( $deck->has52StandardCards() );

		$this->assertFalse( $before === $deck->asLetters() );
	}



	public function testOverhandFineShuffle()
	{
		$deck = Deck::make()->setBicycle();

		$before = $deck->asLetters();

		$deck->shuffleOverhandFine();

		$this->assertTrue( $deck->has52StandardCards() );

		$this->assertFalse( $before === $deck->asLetters() );
	}
}