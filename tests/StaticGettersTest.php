<?php
Namespace dgifford\Deck\Tests;



use dgifford\Deck\Deck;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class StaticGettersTest extends \PHPUnit\Framework\TestCase
{
	public function testStandard52Chased()
	{
		$this->assertSame(
        [
            'ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            'ah', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', 'jh', 'qh', 'kh',
            'as', '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', '10s', 'js', 'qs', 'ks',
            'ad', '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', '10d', 'jd', 'qd', 'kd',
        ],
            Deck::standard52Chased()
        );
	}



	public function testSuit()
	{
		$this->assertSame(
        [
            'ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
        ],
            Deck::suit('clubs')
        );

		$this->assertSame(
        [
            'ah', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', 'jh', 'qh', 'kh',
        ],
            Deck::suit('hearts')
        );

		$this->assertSame(
        [
            'as', '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', '10s', 'js', 'qs', 'ks',
        ],
            Deck::suit('spades')
        );

		$this->assertSame(
        [
            'ad', '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', '10d', 'jd', 'qd', 'kd',
        ],
            Deck::suit('diamonds')
        );
	}



	public function testInvalidSuit()
	{
		$this->expectException( \InvalidArgumentException::class );

        Deck::suit('foo');
	}



	public function testSuitLetters()
	{
        $this->assertSame( ['c','h','s','d'], Deck::suitLetters() );
	}



	public function testValueLetters()
	{
        $this->assertSame(
        [
            'a', '2', '3', '4', '5', '6', '7', '8', '9', '10', 'j', 'q', 'k',
        ],
            Deck::valueLetters()
        );
	}



}