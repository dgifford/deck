<?php
Namespace dgifford\Deck\Tests;



use dgifford\Deck\Deck;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class SelectionTest extends \PHPUnit\Framework\TestCase
{
	public function testSelectCards()
	{
		$deck = Deck::make()->setClubs();

		$this->assertSame( [], $deck->findSelected() );

		$deck->select([ 1, 2, ]);

		$this->assertSame([ 1, 2, ], $deck->findSelected() );
	}



	public function testSelectAllCards()
	{
		$deck = Deck::make()->setClubs();

		$this->assertSame( [], $deck->findSelected() );

		$deck->select();

		$this->assertSame( 13, count($deck->findSelected()) );
	}



	public function testDeselectAllCards()
	{
		$deck = Deck::make()->setClubs();

		$deck->select();

		$this->assertSame( 13, count($deck->findSelected()) );

		$deck->deselect();

		$this->assertSame( 0, count($deck->findSelected()) );
	}



	public function testDeselectCards()
	{
		$deck = Deck::make()->setClubs();

		$deck->select();

		$this->assertSame( 13, count($deck->findSelected()) );

		$deck->deselect([ 0, 1, ]);

		$this->assertSame( 11, count($deck->findSelected()) );

		$this->assertFalse( $deck[0]->getSelected() );

		$this->assertFalse( $deck[1]->getSelected() );
	}


	public function testSelectOnly()
	{
		$deck = Deck::make()->setClubs();

		$deck->select();

		$this->assertSame( 13, count($deck->findSelected()) );

		$deck->selectOnly([ 0, 1, ]);

		$this->assertSame( 2, count($deck->findSelected()) );

		$this->assertTrue( $deck[0]->getSelected() );

		$this->assertTrue( $deck[1]->getSelected() );
	}


}