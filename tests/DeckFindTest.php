<?php
Namespace dgifford\Deck\Tests;



use dgifford\Deck\Deck;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class DeckFindTest extends \PHPUnit\Framework\TestCase
{
	public function testFind()
	{
		$deck = Deck::make()->setBicycle();

		$this->assertSame( [2], $deck->find('ah') );

		$this->assertSame( [53], $deck->find('as') );

		$this->assertSame( [], $deck->find('xx') );
	}



	public function testFindByFace()
	{
		$deck = Deck::make()->setClubs();

		$this->assertSame( [0], $deck->find('ac') );

		$deck->append( Deck::suit('clubs') );

		$this->assertSame( [0, 13], $deck->find('ac') );

		$this->assertSame( [], $deck->find('ah') );
	}



	public function testFindByBack()
	{
		$deck = Deck::make()->setClubs();

		$this->assertSame( [0,1,2,3,4,5,6,7,8,9,10,11,12,], $deck->find('bb') );

		$deck->append([ 'rb:2c' ]);

		$this->assertSame( [13], $deck->find('rb') );

		$this->assertSame( [1, 13], $deck->find('2c') );
	}



	public function testFindByID()
	{
		$deck = Deck::make()->setClubs();

		$this->assertSame( [0], $deck->find( 0 ) );

		$this->assertSame( [], $deck->find( 13 ) );

		$deck->append( Deck::suit('clubs') );

		$this->assertSame( [13], $deck->find( 13 ) );
	}



	public function testFindByArray()
	{
		$deck = Deck::make()->setClubs();
		
		$deck[0]->setSelected(true);

		$deck[1]->setSelected(true);
		
		$deck[1]->setReversed(true);	

		$this->assertSame( [1], $deck->find(['front' => '2c' ]) );

		$this->assertSame( [0,1,2,3,4,5,6,7,8,9,10,11,12], $deck->find(['back' => 'bb']));

		$this->assertSame( [0,1], $deck->find(['selected' => true ]) );

		$this->assertSame( [1], $deck->find(['reversed' => true, 'selected' => true ]) );

		$this->assertSame( [0], $deck->find(['reversed' => false, 'selected' => true ]) );

		$this->assertSame( [], $deck->find(['reversed' => true, 'revolved' => true ]) );
	}



    public function testFindSelected()
    {
        $deck = Deck::make()->setClubs();

        $deck[1]->setSelected(true);

        $deck[4]->setSelected(true);

        $this->assertSame( [1,4], $deck->findSelected() );
    }



    public function testFindReversed()
    {
        $deck = Deck::make()->setClubs();

        $deck[1]->setReversed(true);

        $deck[4]->setReversed(true);

        $this->assertSame( [1,4], $deck->findReversed() );
    }



    public function testFindRevolved
    ()
    {
        $deck = Deck::make()->setClubs();

        $deck[1]->setRevolved(true);

        $deck[4]->setRevolved(true);

        $this->assertSame( [1,4], $deck->findRevolved() );
    }
}