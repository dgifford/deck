<?php

Namespace dgifford\Deck\Tests;


use dgifford\Deck\Card;
use dgifford\Deck\Deck;


/**
 * Auto Loader
 *
 */
require_once(__DIR__ . '/../vendor/autoload.php');


class DeckTest extends \PHPUnit\Framework\TestCase
{
    public function testInstantiateDeck()
    {
        $deck = new Deck;

        $this->assertTrue($deck instanceof Deck);
    }


    public function testInstantiateDeckWithArray()
    {
        $deck = new Deck(['ac', 'ah', 'as', 'ad',]);

        $this->assertTrue($deck instanceof Deck);

        $this->assertSame('ac', $deck[0]->getFront());

        $this->assertSame('bb', $deck[0]->getBack());

        $this->assertSame(['ac', 'ah', 'as', 'ad',], $deck->asLetters());
    }


    public function testInstantiateDeckWithMixedArray()
    {
        $card = new Card(['front' => 'ah', 'back' => 'rb', 'reversed' => true]);

        $deck = new Deck(['ac', $card, 'as:rb',]);

        $this->assertTrue($deck instanceof Deck);

        $this->assertSame('ac', $deck[0]->getFront());

        $this->assertSame('ah', $deck[1]->getFront());

        $this->assertSame('rb', $deck[1]->getBack());

        $this->assertTrue($deck[1]->getReversed());

        $this->assertFalse($deck[1]->getRevolved());

        $this->assertSame('as', $deck[2]->getFront());

        $this->assertSame('rb', $deck[2]->getBack());

        $this->assertSame(3, $deck->count());
    }


    public function testDefaultDeckIsEmpty()
    {
        $deck = new Deck;

        $this->assertSame([], $deck->asLetters());

        $this->assertSame(0, $deck->count());
    }


    public function testIsComplete()
    {
        $deck = Deck::make()->setBicycle();

        $this->assertTrue($deck->has52StandardCards());

        unset($deck[2]);

        $this->assertFalse($deck->has52StandardCards());
    }


    public function testGetPercent()
    {
        $deck = Deck::make()->setBicycle();

        $this->assertSame(56, $deck->getPercent(100));

        $this->assertSame(28, $deck->getPercent(50));

        $this->assertSame(14, $deck->getPercent(25));

        $this->assertSame(5, $deck->getPercent(10));

        $this->assertSame(6, $deck->getPercent(10, true));
    }


    public function testGetIndex()
    {
        $deck = Deck::make()->setBicycle();

        $this->assertSame(0, $deck->getIndex(0));

        $this->assertSame(10, $deck->getIndex(10));
    }


    public function testMoveCardDown()
    {
        $deck = Deck::make()->setBicycle();

        $deck->move([0], 2);

        $this->assertSame('p2', $deck[0]->front());
        $this->assertSame('ah', $deck[1]->front());
        $this->assertSame('p1', $deck[2]->front());
    }


    public function testMoveCardUp()
    {
        $deck = Deck::make()->setBicycle();

        $deck->move([25], 1);

        $this->assertSame('p1', $deck[0]->front());
        $this->assertSame('jc', $deck[1]->front());
        $this->assertSame('10c', $deck[25]->front());
        $this->assertSame('j2', $deck[55]->front());
    }


    public function testMoveCardDownTwice()
    {
        $deck = Deck::make()->setClubs();

        $deck->move([0], 1);

        $this->assertSame(['2c', 'ac', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());

        $deck->move([0], 2);

        $this->assertSame(['ac', '3c', '2c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());
    }


    public function testMoveContinuousBlockOfCardsToStart()
    {
        $deck = Deck::make()->setClubs();

        $deck->move([2, 3, 4], 0);

        $this->assertSame(['3c', '4c', '5c', 'ac', '2c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());

        $this->assertSame('5c', $deck[2]->front());
    }


    public function testMoveContinuousBlockOfCardsToEnd()
    {
        $deck = Deck::make()->setClubs();

        $deck->move([2, 3, 4], 12);

        $this->assertSame(['ac', '2c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc', '3c', '4c', '5c',], $deck->asLetters());

        $this->assertSame('6c', $deck[2]->front());
    }


    public function testMoveContinuousBlockOfCardsToInvalidDestination()
    {
        $deck = Deck::make()->setClubs();

        $this->assertFalse($deck->move([2, 3, 4], 100));
    }


    public function testMoveInvalidBlockOfCardsToValidDestination()
    {
        $deck = Deck::make()->setClubs();

        $this->assertFalse($deck->move([2, 3, 100], 0));
    }


    public function testMoveDiscontinuousBlockOfCardsToStart()
    {
        $deck = Deck::make()->setClubs();

        $deck->move([0, 2, 12], 0);

        $this->assertSame(['ac', '3c', 'kc', '2c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc',], $deck->asLetters());

        $this->assertSame('kc', $deck[2]->front());
    }


    public function testIsValidIndex()
    {
        $deck = Deck::make()->setBicycle();

        $this->assertTrue($deck->isValidIndex(0));
        $this->assertTrue($deck->isValidIndex(53));
        $this->assertFalse($deck->isValidIndex(-10));
        $this->assertFalse($deck->isValidIndex(58));
    }


    public function testIsValidID()
    {
        $deck = Deck::make()->setBicycle();

        $this->assertTrue($deck->isValidID(53));
        $this->assertFalse($deck->isValidID(-10));
    }


    public function testIsValidType()
    {
        $this->assertTrue(Deck::isValidType('club'));
        $this->assertFalse(Deck::isValidType('foo'));
    }


    public function testIsStandardFace()
    {
        $this->assertTrue(Deck::isStandardFace('2c'));
        $this->assertTrue(Deck::isStandardFace('j1'));
        $this->assertFalse(Deck::isStandardFace('foo'));
    }


    public function testIsSuitedFace()
    {
        $this->assertTrue(Deck::isSuitedFace('2c'));
        $this->assertFalse(Deck::isSuitedFace('j1'));
        $this->assertFalse(Deck::isSuitedFace('13c'));
    }


    public function testIsValidSuit()
    {
        $this->assertTrue(Deck::isValidSuit('c'));
        $this->assertTrue(Deck::isValidSuit('clubs'));
        $this->assertFalse(Deck::isValidSuit('foo'));
    }


    public function testMoveCardInvalidDestination()
    {
        $deck = Deck::make()->setBicycle();

        $deck->move([53], 100);

        $this->assertSame('p1', $deck[0]->front());
        $this->assertSame('j2', $deck[55]->front());
        $this->assertSame(56, $deck->count());
    }


    public function testMoveCardDestinationTooLow()
    {
        $deck = Deck::make()->setBicycle();

        $deck->move([53], -10);

        $this->assertSame('p1', $deck[0]->front());
        $this->assertSame('j2', $deck[55]->front());
        $this->assertSame(56, $deck->count());
    }


    public function testMoveCardDestinationTooHigh()
    {
        $deck = Deck::make()->setBicycle();

        $deck->move([53], 100);

        $this->assertSame('p1', $deck[0]->front());
        $this->assertSame('j2', $deck[55]->front());
        $this->assertSame(56, $deck->count());
    }


    public function testClubs()
    {
        $deck = Deck::make()->setClubs();

        $this->assertSame(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());
    }


    public function testClear()
    {
        $deck = Deck::make()->setClubs();

        $deck->clear();

        $this->assertSame([], $deck->asLetters());
    }


    public function testAddjokers()
    {
        $deck = Deck::make()->setClubs();

        $deck->addJokers();

        $this->assertSame(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc', 'j1', 'j2'], $deck->asLetters());
    }


    public function testRevolvedCards()
    {
        $deck = Deck::make()->setClubs();

        $this->assertFalse($deck->getCard(0)->getRevolved());
        $this->assertFalse($deck->getCard(3)->getRevolved());
        $this->assertFalse($deck->getCard(5)->getRevolved());

        $deck->revolve([0, 3, 5]);

        $this->assertTrue($deck->getCard(0)->getRevolved());
        $this->assertTrue($deck->getCard(3)->getRevolved());
        $this->assertTrue($deck->getCard(5)->getRevolved());

        $deck->derevolve([0, 3, 5]);

        $this->assertFalse($deck->getCard(0)->getRevolved());
        $this->assertFalse($deck->getCard(3)->getRevolved());
        $this->assertFalse($deck->getCard(5)->getRevolved());
    }


    public function testReversedCards()
    {
        $deck = Deck::make()->setClubs();

        $this->assertFalse($deck->getCard(0)->getReversed());
        $this->assertFalse($deck->getCard(3)->getReversed());
        $this->assertFalse($deck->getCard(5)->getReversed());

        $deck->reverse([0, 3, 5]);

        $this->assertTrue($deck->getCard(0)->getReversed());
        $this->assertTrue($deck->getCard(3)->getReversed());
        $this->assertTrue($deck->getCard(5)->getReversed());

        $deck->dereverse([0, 3, 5]);

        $this->assertFalse($deck->getCard(0)->getReversed());
        $this->assertFalse($deck->getCard(3)->getReversed());
        $this->assertFalse($deck->getCard(5)->getReversed());
    }


    public function testToggleRevolvedCards()
    {
        $deck = Deck::make()->setClubs();

        $this->assertFalse($deck->getCard(0)->getRevolved());
        $this->assertFalse($deck->getCard(3)->getRevolved());
        $this->assertFalse($deck->getCard(5)->getRevolved());

        $deck->toggleRevolved([0, 3, 5]);

        $this->assertTrue($deck->getCard(0)->getRevolved());
        $this->assertTrue($deck->getCard(3)->getRevolved());
        $this->assertTrue($deck->getCard(5)->getRevolved());

        $deck->toggleRevolved([0, 3, 5]);

        $this->assertFalse($deck->getCard(0)->getRevolved());
        $this->assertFalse($deck->getCard(3)->getRevolved());
        $this->assertFalse($deck->getCard(5)->getRevolved());
    }


    public function testToggleReversedCards()
    {
        $deck = Deck::make()->setClubs();

        $this->assertFalse($deck->getCard(0)->getReversed());
        $this->assertFalse($deck->getCard(3)->getReversed());
        $this->assertFalse($deck->getCard(5)->getReversed());

        $deck->toggleReversed([0, 3, 5]);

        $this->assertTrue($deck->getCard(0)->getReversed());
        $this->assertTrue($deck->getCard(3)->getReversed());
        $this->assertTrue($deck->getCard(5)->getReversed());

        $deck->toggleReversed([0, 3, 5]);

        $this->assertFalse($deck->getCard(0)->getReversed());
        $this->assertFalse($deck->getCard(3)->getReversed());
        $this->assertFalse($deck->getCard(5)->getReversed());
    }


    public function testToggleSelectedCards()
    {
        $deck = Deck::make()->setClubs();

        $this->assertFalse($deck->getCard(0)->getSelected());
        $this->assertFalse($deck->getCard(3)->getSelected());
        $this->assertFalse($deck->getCard(5)->getSelected());

        $deck->toggleSelected([0, 3, 5]);

        $this->assertTrue($deck->getCard(0)->getSelected());
        $this->assertTrue($deck->getCard(3)->getSelected());
        $this->assertTrue($deck->getCard(5)->getSelected());

        $deck->toggleSelected([0, 3, 5]);

        $this->assertFalse($deck->getCard(0)->getSelected());
        $this->assertFalse($deck->getCard(3)->getSelected());
        $this->assertFalse($deck->getCard(5)->getSelected());
    }


    public function testSetWithValidCards()
    {
        $deck = Deck::make()->setBicycle();

        $result = $deck->set(['ac', 'ah', 'as', 'ad',]);

        $this->assertSame([true, true, true, true,], $result);

        $this->assertSame(['ac', 'ah', 'as', 'ad',], $deck->asLetters());

        $this->assertSame(3, $deck->findID(3));

        $this->assertFalse($deck->findID(5));
    }


    public function testSetWithInvalidCards()
    {
        $deck = Deck::make()->setBicycle();

        $result = $deck->set(['bl', false, [], 'ad',]);

        $this->assertSame([true, false, false, true], $result);

        $this->assertSame(['bl', 'ad',], $deck->asLetters());
    }


    public function testAddSuitOfCards()
    {
        $deck = Deck::make()->setClubs();

        $this->assertSame(13, $deck->count());

        $deck->append(Deck::suit('diamonds'));

        $this->assertSame(26, $deck->count());

        $this->assertSame(25, $deck->findID(25));
    }


    public function testAddCardsWithTextArray()
    {
        $deck = Deck::make()->setClubs();

        $this->assertSame(13, $deck->count());

        $deck->append(['ac', 'ad', 'ah', 'as']);

        $this->assertSame(17, $deck->count());

        $this->assertSame('ac', $deck[13]->getFace());
    }


    public function testAddInvalidCards()
    {
        $deck = new Deck;

        $this->assertSame(0, $deck->count());

        $result = $deck->append(['ac', '', 'ad', false, [], 'xx', 'ah', 'as']);

        $this->assertSame(5, $deck->count());

        $this->assertSame([true, false, true, false, false, true, true, true,], $result);
    }


    public function testAddAtStart()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->add([0 => 'ah']);

        $this->assertSame([0 => true], $result);

        $this->assertSame(
            [
                'ah', 'ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters());
    }


    public function testAddPartWayThrough()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->add([5 => 'ah']);

        $this->assertSame([0 => true], $result);

        $this->assertSame(
            [
                'ac', '2c', '3c', '4c', '5c', 'ah', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters());
    }


    public function testAddMultipleCardsConsecutively()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->add([5 => 'ah', 6 => '2h']);

        $this->assertSame([true, true], $result);

        $this->assertSame(
            [
                'ac', '2c', '3c', '4c', '5c', 'ah', '2h', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters());
    }


    public function testAddMultipleCardsWithSomeInvalid()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->add([5 => 'ah', 8 => false, 6 => '2h']);

        $this->assertSame([true, false, true], $result);


        $this->assertSame(
            [
                'ac', '2c', '3c', '4c', '5c', 'ah', '2h', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters());
    }


    public function testIDsCorrectAfterAddingCards()
    {
        $deck = Deck::make()->setClubs();

        $this->assertSame(12, $deck->getMaxID());

        $result = $deck->add([5 => 'ah', 6 => '2h', 8 => '3h']);

        $this->assertSame(15, $deck->getMaxID());
    }


    public function testShiftCardFromTop()
    {
        $deck = Deck::make()->setClubs();

        $card = $deck->shift();

        $this->assertSame('ac', $card->getFace());

        $this->assertSame('2c', $deck[0]->getFace());

        $this->assertSame(12, $deck->count());
    }


    public function testGetJson()
    {
        $deck = Deck::make(['ac', '2c']);

        $this->assertSame(
            '[{"id":1,"value":"a","suit":"c","front":"ac","back":"bb","revolved":false,"reversed":false,"type":"club","selected":false},{"id":2,"value":"2","suit":"c","front":"2c","back":"bb","revolved":false,"reversed":false,"type":"club","selected":false}]'
            , $deck->getJson()
        );
    }


    public function testCopyCards()
    {
        $deck = Deck::make()->setClubs();

        $cards = $deck->copy([0, 2, 4]);

        $this->assertSame([0, 2, 4], array_keys($cards));

        $this->assertSame('5c', $cards[4]->getFront());
    }
}
