<?php

Namespace dgifford\Deck\Tests;


use dgifford\Deck\Deck;


/**
 * Auto Loader
 *
 */
require_once(__DIR__ . '/../vendor/autoload.php');


class DealingTest extends \PHPUnit\Framework\TestCase
{
    public function testDealOutOneHandWithoutReassembling()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->deal(1, null, false);

        $this->assertCount( 1, $result['hands'] );

        $this->assertSame(
            ['kc', 'qc', 'jc', '10c', '9c', '8c', '7c', '6c', '5c', '4c', '3c', '2c','ac', ]
            , $result['hands'][0]->asLetters()
        );

        $this->assertSame( 0, $result['stock']->count() );
    }


    public function testDealOutTwoHandsWithoutReassembling()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->deal(2, null, false);

        $this->assertCount( 2, $result['hands'] );

        $this->assertSame(
            ['kc', 'jc', '9c', '7c', '5c', '3c', 'ac', ]
            , $result['hands'][0]->asLetters()
        );

        $this->assertSame(
            ['qc', '10c', '8c', '6c', '4c', '2c',]
            , $result['hands'][1]->asLetters()
        );

        $this->assertSame( 0, $result['stock']->count() );
    }


    public function testDealOutTwoHands()
    {
        $deck = Deck::make()->setClubs();

        $deck->deal(2);

        $this->assertSame(
            ['qc', '10c', '8c', '6c', '4c', '2c', 'kc', 'jc', '9c', '7c', '5c', '3c', 'ac', ]
            , $deck->asLetters()
        );
    }


    public function testDealThreeHandsOfThreeWithoutReassembling()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->deal(3, 3, false);

        $this->assertCount( 3, $result['hands'] );

        $this->assertSame(
            ['7c', '4c', 'ac',]
            , $result['hands'][0]->asLetters()
        );

        $this->assertSame(
            ['8c', '5c', '2c',]
            , $result['hands'][1]->asLetters()
        );

        $this->assertSame(
            ['9c', '6c', '3c',]
            , $result['hands'][2]->asLetters()
        );

        $this->assertSame(
            ['10c', 'jc', 'qc', 'kc',]
            , $result['stock']->asLetters()
        );
    }


    public function testDealThreeHandsOfThree()
    {
        $deck = Deck::make()->setClubs();

        $deck->deal(3, 3);

        $this->assertSame(
            ['9c', '6c', '3c', '8c', '5c', '2c', '7c', '4c', 'ac', '10c', 'jc', 'qc', 'kc', ]
            , $deck->asLetters()
        );
    }


    public function testDealFourHandsOfFiveWithoutReassembling()
    {
        $deck = Deck::make()->setSuited();

        $result = $deck->deal(3, 5, false);

        $this->assertCount( 3, $result['hands'] );

        $this->assertSame(
            ['kc', '10c', '7c', '4c', 'ac',]
            , $result['hands'][0]->asLetters()
        );

        $this->assertSame(
            ['ah', 'jc', '8c', '5c', '2c',]
            , $result['hands'][1]->asLetters()
        );

        $this->assertSame(
            ['2h', 'qc', '9c', '6c', '3c',]
            , $result['hands'][2]->asLetters()
        );

        $this->assertSame(
            37
            , $result['stock']->count()
        );
    }


    public function testDealThreeHandsWithLimitHigherThanTotalCardsWithoutReassembling()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->deal(3, 5, false);

        $this->assertCount( 3, $result['hands'] );

        $this->assertSame(
            ['kc', '10c', '7c', '4c', 'ac',]
            , $result['hands'][0]->asLetters()
        );

        $this->assertSame(
            ['jc', '8c', '5c', '2c',]
            , $result['hands'][1]->asLetters()
        );

        $this->assertSame(
            ['qc', '9c', '6c', '3c',]
            , $result['hands'][2]->asLetters()
        );

        $this->assertSame(
            0
            , $result['stock']->count()
        );
    }


    public function testDealMoreHandsThanTotalCardsWithoutReassembling()
    {
        $deck = Deck::make(['ac', '2c', '3c', '4c', '5c', '6c',]);

        $result = $deck->deal(8, 5, false);

        $this->assertCount( 8, $result['hands'] );

        $this->assertSame(
            ['ac',]
            , $result['hands'][0]->asLetters()
        );

        $this->assertSame(
            ['2c',]
            , $result['hands'][1]->asLetters()
        );

        $this->assertSame(
            ['3c',]
            , $result['hands'][2]->asLetters()
        );

        $this->assertSame(
            ['4c',]
            , $result['hands'][3]->asLetters()
        );

        $this->assertSame(
            ['5c',]
            , $result['hands'][4]->asLetters()
        );

        $this->assertSame(
            ['6c',]
            , $result['hands'][5]->asLetters()
        );

        $this->assertSame(
            []
            , $result['hands'][6]->asLetters()
        );

        $this->assertSame(
            []
            , $result['hands'][7]->asLetters()
        );

        $this->assertSame(
            0
            , $result['stock']->count()
        );
    }


    public function testRepeatingSquareDealReversesFirstDeal()
    {
        $deck = Deck::make(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c',]);

        $deck->deal(3, 3);

        $deck->deal(3, 3);

        $this->assertSame(
            ['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c',]
            , $deck->asLetters()
        );
    }


    public function testReverseOrder()
    {
        $deck = Deck::make(['ac', '2c', '3c', '4c', '5c',]);

        $deck->reverseOrder();

        $this->assertSame(
            ['5c', '4c', '3c', '2c', 'ac', ]
            , $deck->asLetters()
        );

        $deck->reverseOrder();

        $this->assertSame(
            ['ac', '2c', '3c', '4c', '5c',]
            , $deck->asLetters()
        );
    }
}