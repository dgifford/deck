<?php

Namespace dgifford\Deck\Tests;


use dgifford\Deck\Deck;


/**
 * Auto Loader
 *
 */
require_once(__DIR__ . '/../vendor/autoload.php');


class RandomisationTest extends \PHPUnit\Framework\TestCase
{
    public function testInstantiateDeck()
    {
        $deck = new Deck;

        $this->assertTrue($deck instanceof Deck);
    }


    public function testArrayRandomizeFast()
    {
        $arr = range('A', 'Z');

        $result = Deck::randomizeArray($arr);

        $this->assertFalse($arr === $result);

        $this->assertTrue(is_array($result));

        $this->assertSame(count($arr), count($result));

        foreach ($result as $item) {
            $this->assertTrue(in_array($item, $arr));
        }
    }


    public function testArrayRandomizeSlow()
    {
        $arr = range('A', 'Z');

        $result = Deck::randomizeArray($arr, false);

        $this->assertFalse($arr === $result);

        $this->assertTrue(is_array($result));

        $this->assertSame(count($arr), count($result));

        foreach ($result as $item) {
            $this->assertTrue(in_array($item, $arr));
        }
    }


    /*    public function testRandomizeVisually()
        {
            $this->expectNotToPerformAssertions();

            $arr = str_split('01234567890123456789012345678901234567890123456789', 1);

            echo "\n\nDeck::randomizeArraySlow:\n";

            for( $i=0; $i < 100; $i++ )
            {
                echo implode('', Deck::randomizeArraySlow( $arr ) ) . "\n";
            }

            $arr = str_split('01234567890123456789012345678901234567890123456789', 1);

            echo "\n\nDeck::randomizeArrayFast:\n";

            for( $i=0; $i < 100; $i++ )
            {
                echo implode('', Deck::randomizeArrayFast( $arr ) ) . "\n";
            }

            $arr = str_split('01234567890123456789012345678901234567890123456789', 1);

            echo "\n\nPHP Shuffle:\n";

            for( $i=0; $i < 100; $i++ )
            {
                shuffle($arr);
                echo implode('',  $arr ) . "\n";
            }
        }*/


    public function testRandomizeFunctionTimes()
    {
        $this->expectNotToPerformAssertions();

        $arr = range('A', 'Z');

        $repetitions = 25000;

        $start = microtime(true);
        for ($i = 0; $i < $repetitions; $i++) {
            Deck::randomizeArraySlow($arr);
        }
        $time = microtime(true) - $start;

        echo "\nDeck::randomizeArraySlow {$time} for {$repetitions} repetitions.\n";

        $start = microtime(true);
        for ($i = 0; $i < $repetitions; $i++) {
            Deck::randomizeArrayFast($arr);
        }
        $time = microtime(true) - $start;

        echo "\nDeck::randomizeArrayFast {$time} for {$repetitions} repetitions.\n";

        $start = microtime(true);
        for ($i = 0; $i < $repetitions; $i++) {
            shuffle($arr);
        }
        $time = microtime(true) - $start;

        echo "\nPHP shuffle {$time} for {$repetitions} repetitions.\n";
    }
}