<?php
Namespace dgifford\Deck\Tests;


use dgifford\Deck\Card;
use dgifford\Deck\Deck;



/**
 * Auto Loader
 *
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class RemoveCardsTest extends \PHPUnit\Framework\TestCase
{
    public function testRemoveCardByID()
    {
        $deck = Deck::make()->setBicycle();

        $result = $deck->remove([0]);

        $this->assertSame( 1, count( $result ) );

        $this->assertTrue( $result[0] instanceof Card );

        $this->assertSame( 55, $deck->count() );

        $this->assertSame( 'p2', $deck[0]->front() );
    }



    public function testRemoveCardByInvalidID()
    {
        $deck = Deck::make()->setBicycle();

        $result = $deck->remove([100]);

        $this->assertSame( [100 => false], $result );

        $this->assertSame( 56, $deck->count() );
    }



    public function testRemoveCardNoReindex()
    {
        $deck = Deck::make()->setBicycle();

        $result = $deck->remove([0], false);

        $this->assertSame( 1, count( $result ) );

        $this->assertTrue( $result[0] instanceof Card );

        $this->assertSame( 55, $deck->count() );

        $this->assertSame( 'p2', $deck[1]->front() );
    }



    public function testRemoveConsecutiveBlockOfCards()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->remove([0,1,2,3]);

        $this->assertSame( 4, count( $result ) );

        $this->assertTrue( $result[0] instanceof Card );

        $this->assertSame( 9, $deck->count() );

        $this->assertSame( '5c', $deck[0]->front() );
    }



    public function testRemoveConsecutiveBlockOfCardsNoReindex()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->remove([0,1,2,3], false);

        $this->assertSame( 4, count( $result ) );

        $this->assertTrue( $result[0] instanceof Card );

        $this->assertSame( 9, $deck->count() );

        $this->assertfalse( isset( $deck[0] ) );

        $this->assertSame( '5c', $deck[4]->front() );
    }



    public function testRemoveDiscontinuousBlockOfCards()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->remove([0,3,5]);

        $this->assertSame( 3, count( $result ) );

        $this->assertTrue( $result[0] instanceof Card );

        $this->assertSame( 10, $deck->count() );

        $this->assertfalse( isset( $deck[10] ) );

        $this->assertSame( '2c', $deck[0]->front() );
        $this->assertSame( '3c', $deck[1]->front() );
        $this->assertSame( '5c', $deck[2]->front() );
        $this->assertSame( '7c', $deck[3]->front() );
    }


    public function testRemoveCardsByIndex()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->remove([0,2,4]);

        $this->assertSame(
            [
                '2c', '4c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters() );

        $this->assertSame( [0,2,4], array_keys($result) );

        $this->assertSame( 'ac', $result[0]->getFront() );

        $this->assertSame( '3c', $result[2]->getFront() );

        $this->assertSame( '5c', $result[4]->getFront() );
    }


    public function testRemoveCardsByInvalidIndex()
    {
        $deck = Deck::make()->setClubs();

        $result = $deck->remove([11,12,13]);

        $this->assertSame(
            [
                'ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc',
            ], $deck->asLetters() );

        $this->assertSame( [11,12,13], array_keys($result) );

        $this->assertSame( 'qc', $result[11]->getFront() );

        $this->assertSame( 'kc', $result[12]->getFront() );

        $this->assertFalse( $result[13] );
    }



    public function testRemoveAndReaddCardsByIndex()
    {
        $deck = Deck::make()->setClubs();

        $cards = $deck->remove([0,2,4]);

        $this->assertSame(
            [
                '2c', '4c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters() );

        $deck->add($cards);

        $this->assertSame(
            [
                'ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters() );
    }


    public function testRemoveByID()
    {
        $deck = Deck::make()->setClubs();

        $deck[0]->setID( 99 );

        $cards = $deck->removeByID([99]);

        $this->assertSame(
            [
                '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',
            ], $deck->asLetters() );

        $this->assertSame( 'ac', $cards[0]->getFront());
    }
}
