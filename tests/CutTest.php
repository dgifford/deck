<?php
Namespace dgifford\Deck\Tests;


use dgifford\Deck\Card;
use dgifford\Deck\Deck;



/**
 * Auto Loader
 * 
 */
require_once(__DIR__ . '/../vendor/autoload.php');



class CutTest extends \PHPUnit\Framework\TestCase
{
	public function testCutHalf()
	{
		$deck = Deck::make()->setBicycle();

		$deck->cutHalf();

		$this->assertSame( 56, $deck->count() );

		$this->assertSame( 'kd', $deck[0]->front() );
    }



    public function testRandomCut()
    {
        $deck = Deck::make()->setBicycle();

        $pos = $deck->cutRandom();

        $this->assertTrue( $deck->has52StandardCards() );

        $this->assertSame(
            Deck::make()->setBicycle()[$pos]->front(),
            $deck[0]->front()
        );
    }



    public function testRandomCutOutOfRangeHasNoEffect()
    {
        $deck = Deck::make()->setBicycle();

        $pos = $deck->cutRandom( 80, 90 );

        $this->assertTrue( $deck->has52StandardCards() );

        $this->assertSame(
            Deck::make()->setBicycle()[$pos]->front(),
            $deck[0]->front()
        );
    }



	public function testCutAtAndReassemble()
	{
		$deck = Deck::make()->setBicycle();

		$deck->cutAt(28);

		$this->assertSame( 56, $deck->count() );

		$this->assertSame( 'kd', $deck[0]->front() );
    }



	public function testCutAtWithoutReassembling()
	{
		$deck = Deck::make()->setBicycle();

		$result = $deck->cutAt(28, false);

		$this->assertSame( 28, count($result['top']) );

		$this->assertSame( 28, count($result['bottom']) );
	}



	public function testCutAtInvalidValueHasNoEffect()
	{
		$deck = Deck::make()->setBicycle();

        $result = $deck->cutAt(70, false);

        $this->assertSame( 56, count($result['top']) );

        $this->assertSame( 0, count($result['bottom']) );

        $this->assertSame( 'p1', $deck[0]->front() );

        $this->assertSame( 56, $deck->count() );
    }



    public function testCutMiddleThird()
    {
        $deck = new Deck([
            'ac', '2c', '3c',
            '4c', '5c', '6c', // cut should only happen within these cards
            '7c', '8c', '9c',
        ]);

        for ($i = 0; $i < 20; $i++)
        {
            $test_deck = clone $deck;

            $pos = $test_deck->cutMiddleThird();

            $this->assertFalse( in_array($test_deck[0]->front(), ['ac', '2c', '3c','8c', '9c', ] ) );

            $this->assertSame(
                $deck[$pos]->front(),
                $test_deck[0]->front()
            );
        }

    }
}