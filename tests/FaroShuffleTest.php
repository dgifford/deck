<?php

Namespace dgifford\Deck\Tests;


use dgifford\Deck\Deck;


/**
 * Auto Loader
 *
 */
require_once(__DIR__ . '/../vendor/autoload.php');


class FaroShuffleTest extends \PHPUnit\Framework\TestCase
{
    public function testEvenNumberedPacketInFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro(6);

        $this->assertSame(['7c', 'ac', '8c', '2c', '9c', '3c', '10c', '4c', 'jc', '5c', 'qc', '6c', 'kc',], $deck->asLetters());

        $deck = Deck::make()->setClubs();

        $deck->shuffleInFaro();

        $this->assertSame(['7c', 'ac', '8c', '2c', '9c', '3c', '10c', '4c', 'jc', '5c', 'qc', '6c', 'kc',], $deck->asLetters());
    }


    public function testOddNumberedPacketInFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro(7);

        $this->assertSame(['8c', 'ac', '9c', '2c', '10c', '3c', 'jc', '4c', 'qc', '5c', 'kc', '6c', '7c',], $deck->asLetters());
    }


    public function testPartialInFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro(2);

        $this->assertSame(['3c', 'ac', '4c', '2c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());
    }


    public function testInvalidCutAtPosition()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro(0);

        $this->assertSame(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());

        $deck->shuffleFaro(50);

        $this->assertSame(['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());
    }


    public function testEvenNumberedPacketOutFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro(6, false);

        $this->assertSame(['ac', '7c', '2c', '8c', '3c', '9c', '4c', '10c', '5c', 'jc', '6c', 'qc', 'kc',], $deck->asLetters());

        $deck = Deck::make()->setClubs();

        $deck->shuffleOutFaro();

        $this->assertSame(['ac', '7c', '2c', '8c', '3c', '9c', '4c', '10c', '5c', 'jc', '6c', 'qc', 'kc',], $deck->asLetters());
    }


    public function testOddNumberedPacketOutFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro(7, false);

        $this->assertSame(['ac', '8c', '2c', '9c', '3c', '10c', '4c', 'jc', '5c', 'qc', '6c', 'kc', '7c',], $deck->asLetters());
    }


    public function testPartialOutFaro()
    {
        $deck = Deck::make()->setClubs();

        $deck->shuffleFaro(2, false);

        $this->assertSame(['ac', '3c', '2c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',], $deck->asLetters());
    }


    public function testEightOutFarosRestoreOrder()
    {
        $deck = Deck::make()->setSuited();

        $before = $deck->asLetters();

        for ($i = 0; $i < 8; $i++) {
            $deck->shuffleOutFaro();
        }

        $this->assertTrue($deck->has52StandardCards());

        $this->assertSame($before, $deck->asLetters());
    }


}