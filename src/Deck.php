<?php

Namespace dgifford\Deck;


/**
 *
 */
class Deck implements \ArrayAccess, \Iterator, \JsonSerializable
{
    use \dgifford\Traits\ArrayAccessTrait;
    use \dgifford\Traits\IteratorTrait;


    /**
     * Card faces indexed by type
     * @var array[]
     */
    public static array $standard_faces =
        [
            // Standard cards by suit
            'club' => ['ac', '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', '10c', 'jc', 'qc', 'kc',],
            'heart' => ['ah', '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', '10h', 'jh', 'qh', 'kh',],
            'spade' => ['as', '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', '10s', 'js', 'qs', 'ks',],
            'diamond' => ['ad', '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', '10d', 'jd', 'qd', 'kd',],

            // Jokers
            'joker' => ['j1', 'j2',],

            // Backs
            'back' => ['rb', 'bb', 'gb', 'yb',],

            // Blank, etc
            'special' => ['bl',],

            // Promotional cards
            'promotional' => ['p1', 'p2',],

            // Alphabet cards
            'alphabet' => ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
                'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',],

            // Number cards
            'number' => ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10',],
        ];


    public static array $suits =
        [
            'c' => 'clubs',
            'h' => 'hearts',
            's' => 'spades',
            'd' => 'diamonds',
        ];


    /**
     * Instantiate a deck with an array of card objects or
     * strings.
     * @param array $cards
     */
    public function __construct(array $cards = [])
    {
        if (!empty($cards)) {
            $this->append($cards);
        }
    }


    /**
     * Static function to instantiate a deck object.
     * @param array $cards
     * @return Deck
     */
    public static function make(array $cards = []): Deck
    {
        return new Deck($cards);
    }


    /**
     * Returns the data for searialisation to JSON
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->container;
    }


    /**
     * Slow array shuffle. Use random_int to randomly shuffle an array.
     *
     * @param array $arr
     * @return array
     * @throws \Exception
     */
    public static function randomizeArraySlow(array $arr): array
    {
        $result = [];

        $keys = array_keys($arr);

        $count = count($arr);

        while ($count > 0) {
            // Choose a random key
            $r = random_int(0, count($keys) - 1);

            // add it to the result
            $result[] = $arr[$keys[$r]];

            // Remove key
            unset($keys[$r]);

            $keys = array_values($keys);

            $count--;
        }

        return $result;
    }


    /**
     * Fast array shuffle. Adds randomness from random_int to array shuffle.
     *
     * @param array $arr
     * @param int $repeat
     * @return array
     * @throws \Exception
     */
    public static function randomizeArrayFast(array $arr, int $repeat = 2): array
    {
        while ($repeat > 0) {
            // Get random index using slow random_int
            $r = random_int(0, count($arr) - 1);

            // Cut at random point
            $bottom = array_slice($arr, $r);
            $top = array_slice($arr, 0, $r);

            // Reassemble
            $arr = array_merge($bottom, $top);

            // Fast shuffle
            shuffle($arr);

            $repeat--;
        }

        return $arr;
    }


    /**
     * Randomize an array using a fast or slow method.
     * @param array $arr
     * @param bool $fast
     * @return array
     * @throws \Exception
     */
    public static function randomizeArray(array $arr, bool $fast = true): array
    {
        if ($fast) {
            return self::randomizeArrayFast($arr);
        }

        return self::randomizeArraySlow($arr);
    }


    /**
     * Find the indexes of cards that match the search term.
     * The term can be a string (matches card fronts and backs),
     * an integer (matches card IDs) or an array
     * (matches all properties set in the array)
     *
     * @param mixed $search Search term
     * @return array
     */
    public function find(mixed $search): array
    {
        $result = [];

        foreach ($this->container as $index => $card) {
            // String search
            if (
                is_string($search)
                and
                ($card->front() == strtolower($search) or $card->back() == strtolower($search))
            ) {
                $result[] = $index;
            } // Integer search
            elseif (
                is_int($search)
                and
                $card->getID() == $search
            ) {
                $result[] = $index;
            } // Array search
            elseif (
                is_array($search)
                and
                array_intersect_assoc($search, $card->jsonSerialize()) == $search
            ) {
                $result[] = $index;
            }
        }

        return $result;
    }


    /**
     * Return the indexes of selected cards
     * @return array
     */
    public function findSelected(): array
    {
        return $this->find(['selected' => true]);
    }


    /**
     * Return the indexes of reversed cards
     * @return array
     */
    public function findReversed(): array
    {
        return $this->find(['reversed' => true]);
    }


    /**
     * Return the indexes of revolved cards
     * @return array
     */
    public function findRevolved(): array
    {
        return $this->find(['revolved' => true]);
    }


    /**
     * Return the index position of a card when
     * given the ID of a card. Legacy method, left
     * for backwards compatibility.
     *
     * @param int $id
     * @return bool|int Index in container
     */
    public function findID(int $id): bool|int
    {
        foreach ($this->container as $key => $card) {
            if ($card->getID() == $id) {
                return $key;
            }
        }

        return false;
    }




    //////////////////////////////////////////////
    // Setters
    //////////////////////////////////////////////


    /**
     * Remove all cards from the deck
     *
     * @return void
     */
    public function clear(): void
    {
        $this->container = [];
    }


    /**
     * Sets the cards in the deck and create new IDs
     * for the cards.
     *
     * @param array $cards
     * @return array $invalid
     */
    public function set(array $cards = []): array
    {
        $this->clear();

        $invalid = $this->append($cards);

        $this->resetIDs();

        return $invalid;
    }


    /**
     * Append an array of card objects or strings onto the
     * deck. Invalid cards are ignored. Card IDs are
     * created. Returns an array of boolean values, where
     * true signifies that a card was valid (and added) and
     * false for invalid cards.
     *
     * @param array|Deck $cards
     * @return array
     */
    public function append(array|Deck $cards): array
    {
        $id = $this->getMaxID();

        $result = [];

        foreach ($cards as $card) {
            if (!$card instanceof Card) {
                try {
                    $card = new Card($card);
                } catch (\InvalidArgumentException $e) {
                    $result[] = false;

                    continue;
                }
            }

            $result[] = true;

            $id++;

            $card->setID($id);

            $this->container[] = $card;
        }

        return $result;
    }


    /**
     * Add an array of cards at positions denoted by their
     * index in the array.
     *
     * @param array $cards
     * @return array
     */
    public function add(array $cards = []): array
    {
        $count = 0;
        $original_count = $this->count();
        $indexes = array_keys($cards);

        // Add cards to end of deck
        $result = $this->append($cards);

        // Move cards successfully added
        foreach ($indexes as $num => $index) {
            if ($result[$num]) {
                $this->move([$original_count + $count], $index);
                $count++;
            }
        }

        return $result;
    }


    /**
     * Add 2 jokers to the deck
     *
     * @return void
     */
    public function addJokers(): void
    {
        $this->append([new Card('j1'), new Card('j2')]);
    }


    /**
     * Remove a card from the deck.
     *
     * @param array $ids
     * @param bool $reindex Whether to reindex from 0 after removal
     * @return array The removed card or false on failure
     */
    public function removeByID(array $ids, bool $reindex = false): array
    {
        $indexes = [];

        foreach ($ids as $id) {
            $indexes[] = $this->getIndex($id);
        }

        return $this->remove($indexes, $reindex);
    }


    /**
     * Remove cards at the given indexes. Returns an array
     * of the removed cards, or false if the index wasn't found.
     * @param array $indexes
     * @param bool $reindex
     * @return array
     */
    public function remove(array $indexes, bool $reindex = true): array
    {
        $cards = [];

        foreach ($indexes as $index) {
            if (isset($this[$index])) {
                $cards[$index] = $this[$index];
                unset($this[$index]);
            } else {
                $cards[$index] = false;
            }
        }

        if ($reindex) {
            $this->reIndex();
        }

        return $cards;
    }


    /**
     * Copy the cards at specific indexes.
     *
     * @param array $indexes
     * @return array
     */
    public function copy(array $indexes): array
    {
        $cards = [];

        foreach ($indexes as $index) {
            if (isset($this[$index])) {
                $cards[$index] = $this[$index];
            } else {
                $cards[$index] = false;
            }
        }

        return $cards;
    }


    /**
     * Shifts the top card off the deck and returns it.
     *
     * @return Card
     */
    public function shift(): Card
    {
        return array_shift($this->container);
    }


    /**
     * Pop a Card of the bottom of the deck.
     *
     * @return Card
     */
    public function pop(): Card
    {
        return array_pop($this->container);
    }


    /**
     * Calls a method (if it exists) on all matching cards
     * with an ID in the IDs array. If no IDs are passed,
     * call the method on all cards.
     *
     * @param string $method
     * @param array $indexes
     * @param array $args
     * @return void
     */
    protected function callOnCards(string $method, array $indexes = [], array $args = []): void
    {
        foreach ($this->container as $index => $card) {
            if (empty($indexes) or in_array($index, $indexes)) {
                call_user_func_array([$card, $method], $args);
            }
        }
    }


    /**
     * Revolve the cards with IDs in the array.
     * @param array $indexes
     * @return void
     */
    public function revolve(array $indexes = []): void
    {
        $this->callOnCards('setRevolved', $indexes, [true]);
    }


    /**
     * Derevolve Cards (set revolved to false)
     * @param array $indexes
     * @return void
     */
    public function derevolve(array $indexes = []): void
    {
        $this->callOnCards('setRevolved', $indexes, [false]);
    }


    /**
     * Toggle the revolved status of the given cards
     * @param array $indexes
     * @return void
     */
    public function toggleRevolved(array $indexes = []): void
    {
        $this->callOnCards('toggleRevolved', $indexes, []);
    }


    /**
     * Reverse the cards with IDs in the passed array.
     * @param array $indexes
     * @return void
     */
    public function reverse(array $indexes = []): void
    {
        $this->callOnCards('setReversed', $indexes, [true]);
    }


    /**
     * Dereverse cards (set reversed to false)
     * @param array $indexes
     * @return void
     */
    public function dereverse(array $indexes = []): void
    {
        $this->callOnCards('setReversed', $indexes, [false]);
    }


    /**
     * Toggle the revolved status of the given cards
     * @param array $indexes
     * @return void
     */
    public function toggleReversed(array $indexes = []): void
    {
        $this->callOnCards('toggleReversed', $indexes, []);
    }


    /**
     * Select the cards with IDs in the passed array.
     * @param array $indexes
     * @return void
     */
    public function select(array $indexes = []): void
    {
        $this->callOnCards('setSelected', $indexes, [true]);
    }


    /**
     * Deselect the cards with IDs in the passed array.
     * @param array $indexes
     * @return void
     */
    public function deselect(array $indexes = []): void
    {
        $this->callOnCards('setSelected', $indexes, [false]);
    }


    /**
     * Toggle the selected status of the given cards
     * @param array $indexes
     * @return void
     */
    public function toggleSelected(array $indexes = []): void
    {
        $this->callOnCards('toggleSelected', $indexes, []);
    }


    /**
     * Select only the indexes provided
     * @param array $indexes
     * @return void
     */
    public function selectOnly(array $indexes = []): void
    {
        $this->deselect();

        $this->select($indexes);
    }


    /**
     * Move cards at the given indexes to the destination
     * index.
     * @param array $indexes
     * @param int $destination
     * @return bool
     */
    public function move(array $indexes, int $destination): bool
    {
        if ($this->isValidIndex($destination)) {
            $cards = $this->remove($indexes, false);

            if (!in_array(false, $cards)) {
                $this->container = array_merge(
                    array_slice($this->container, 0, $destination),
                    $cards,
                    array_slice($this->container, $destination)
                );

                return true;
            }
        }

        return false;
    }


    /**
     * Index the deck from 0.
     *
     * @return void
     */
    public function reIndex(): void
    {
        $this->container = array_values($this->container);
    }


    /**
     * Reset the IDs of the cards so they match their
     * index in the deck.
     * @return void
     */
    public function resetIDs(): void
    {
        foreach ($this->container as $index => $card) {
            $card->setID($index);
        }
    }






    //////////////////////////////////////////////
    // Setters for predefined orders
    //////////////////////////////////////////////


    /**
     * Set the deck to contain clubs.
     * @return $this
     */
    public function setClubs(): Deck
    {
        $this->clear();

        $this->append(self::$standard_faces['club']);

        $this->resetIDs();

        return $this;
    }


    /**
     * Set the deck to contain hearts.
     * @return $this
     */
    public function setHearts(): Deck
    {
        $this->clear();

        $this->append(self::$standard_faces['heart']);

        $this->resetIDs();

        return $this;
    }


    /**
     * Set the deck to contain spades.
     * @return $this
     */
    public function setSpades(): Deck
    {
        $this->clear();

        $this->append(self::$standard_faces['spade']);

        $this->resetIDs();

        return $this;
    }


    /**
     * Set the deck to contain diamonds.
     * @return $this
     */
    public function setDiamonds(): Deck
    {
        $this->clear();

        $this->append(self::$standard_faces['diamond']);

        $this->resetIDs();

        return $this;
    }


    /**
     * Set the deck to standard Bicycle order.
     * @return $this
     */
    public function setBicycle(): Deck
    {
        $this->clear();

        $this->append(self::$standard_faces['promotional']);
        $this->append(self::$standard_faces['heart']);
        $this->append(self::$standard_faces['club']);
        $this->append(array_reverse(self::$standard_faces['diamond']));
        $this->append(array_reverse(self::$standard_faces['spade']));
        $this->append(self::$standard_faces['joker']);

        $this->resetIDs();

        return $this;
    }


    /**
     * Set the deck to standard Copag order.
     * @return $this
     */
    public function setCopag(): Deck
    {
        $this->clear();

        $this->append(['bb:bb', 'bl', 'j1']);
        $this->append(self::$standard_faces['heart']);
        $this->append(self::$standard_faces['club']);
        $this->append(array_reverse(self::$standard_faces['diamond']));
        $this->append(array_reverse(self::$standard_faces['spade']));
        $this->append(['j2']);

        $this->resetIDs();

        return $this;
    }


    /**
     * Set in Alphabet deck order
     * @return $this
     */
    public function setAlphabet(): Deck
    {
        $this->clear();

        $this->append([
            'a', 'a', 'a',
            'b', 'b',
            'c', 'c',
            'd', 'd',
            'e', 'e', 'e',
            'f', 'f',
            'g', 'g',
            'h', 'h',
            'i', 'i',
            'j', 'j',
            'k', 'k',
            'l', 'l',
            'm', 'm',
            'n', 'n',
            'o', 'o',
            'p', 'p',
            'q',
            'r', 'r',
            's', 's',
            't', 't', 't',
            'u', 'u',
            'v', 'v',
            'w', 'w',
            'x',
            'y', 'y',
            'z',
        ]);

        $this->resetIDs();

        return $this;
    }


    /**
     * Set the deck to 52 standard cards in suit order.
     * @return $this
     */
    public function setSuited(): Deck
    {
        $this->clear();

        $this->append(self::standard52Chased());

        $this->resetIDs();

        return $this;
    }


    /**
     * Set the deck to 52 standard cards in random order.
     * @return $this
     * @throws \Exception
     */
    public function setRandom(): Deck
    {
        $this->setSuited();

        $this->shuffleRandom();

        return $this;
    }


    /**
     * Add valid faces to the deck
     * @return $this
     */
    public function setAll(): Deck
    {
        $this->clear();

        foreach (self::$standard_faces as $type => $faces) {
            $this->add($faces);
        }

        return $this;
    }







    //////////////////////////////////////////////
    // Boolean
    //////////////////////////////////////////////


    /**
     * Return true if the given index is valid.
     *
     * @param int $index
     * @return bool
     */
    public function isValidIndex(int $index): bool
    {
        return isset($this->container[$index]);
    }


    /**
     * Return true if the given id is valid.
     *
     * @param int $id
     * @return bool
     */
    public function isValidID(int $id): bool
    {
        if ($this->getIndex($id) === false) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Returns true if the deck contains at least one of each standard
     * cards. Does not check for jokers, duplicates etc.
     *
     * @return bool
     */
    public function has52StandardCards(): bool
    {
        // Check at least 52 cards included
        if ($this->count() < 52) {
            return false;
        }

        foreach (self::standard52Chased() as $face) {
            if (count($this->find($face)) != 1) {
                return false;
            }
        }

        return true;
    }


    /**
     * Boolean check if a type of card face is valid.
     *
     * @param string $type
     * @return bool
     */
    public static function isValidType(string $type): bool
    {
        return isset(self::$standard_faces[$type]);
    }


    /**
     * Boolean check if a face is a standard, e.g. contained
     * in the faces defined by Deck class
     * @param string $face
     * @return bool
     */
    public static function isStandardFace(string $face): bool
    {
        foreach (self::$standard_faces as $faces) {
            if (in_array($face, $faces)) {
                return true;
            }
        }

        return false;
    }


    /**
     * Return true if face has a suit
     * @param string $face
     * @return bool
     */
    public static function isSuitedFace(string $face): bool
    {
        return
            in_array($face, Deck::$standard_faces['club'])
            or in_array($face, Deck::$standard_faces['heart'])
            or in_array($face, Deck::$standard_faces['spade'])
            or in_array($face, Deck::$standard_faces['diamond']);
    }


    /**
     * Boolean cas-insensitive check if a string is a valid suit letter or word.
     *
     * @param string $suit
     * @return bool
     */
    public static function isValidSuit(string $suit): bool
    {
        $suit = strtolower($suit);

        return isset(self::$suits[$suit]) or in_array($suit, self::$suits);
    }





    //////////////////////////////////////////////
    // Getters
    //////////////////////////////////////////////


    /**
     * Given a decimal whole number percentage, return
     * the equivalent number of cards from the current deck.
     *
     * The number of cards is rounded down unless round_up is true.
     *
     * @param int $percent
     * @param bool $round_up
     * @return int
     */
    public function getPercent(int $percent, bool $round_up = false): int
    {
        if ($round_up) {
            $n = ceil($this->count() * ($percent / 100));
        } else {
            $n = floor($this->count() * ($percent / 100));
        }

        return intval($n);
    }


    /**
     * Return the deck as an array of letters
     *
     * @return array
     */
    public function asLetters(): array
    {
        $result = [];

        foreach ($this->container as $card) {
            $result[] = $card->front();
        }

        return $result;
    }


    /**
     * Return the deck as an array of codes.
     *
     * @return array
     */
    public function asCodes(): array
    {
        $result = [];

        foreach ($this->container as $card) {
            $result[] = $card->getCode();
        }

        return $result;
    }


    /**
     * Return a count of the number of cards
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->container);
    }


    /**
     * Return container as jSON
     *
     * @return string
     */
    public function getJson(): string
    {
        return json_encode($this->container);
    }


    /**
     * Return the index of a card if the given id is
     * valid, otherwise false.
     *
     * @param int $id
     * @return int|false
     */
    public function getIndex(int $id): int|false
    {
        foreach ($this->container as $i => $card) {
            /** @var Card $card */
            if ($card->getID() == $id) {
                return $i;
            }
        }

        return false;
    }


    /**
     * Return the maximum ID used in the Deck.
     *
     * @return int|null
     */
    public function getMaxID(): int|null
    {
        $max = 0;

        foreach ($this->container as $card) {

            /** @var Card $card */
            if ($card->getID() > $max) {
                $max = $card->getID();
            }
        }

        return $max;
    }


    /**
     * Return the card with the given id.
     *
     * @param int $id
     * @return Card
     */
    public function getCard(int $id): Card
    {
        if (($index = $this->getIndex($id)) !== false) {
            return $this->container[$index];
        }

        throw new \InvalidArgumentException('Invalid card ID');
    }


    /**
     * Return array of all card IDs.
     *
     * @return array
     */
    public function getAllIDs(): array
    {
        $ids = [];

        foreach ($this->container as $card) {
            $ids[] = $card->getID();
        }

        return $ids;
    }



    //////////////////////////////////////////////
    // Static Getters
    //////////////////////////////////////////////


    /**
     * Return array of 52 standard faces in CHaSeD order
     * @return array
     */
    public static function standard52Chased(): array
    {
        return array_merge(
            self::$standard_faces['club'],
            self::$standard_faces['heart'],
            self::$standard_faces['spade'],
            self::$standard_faces['diamond']
        );
    }


    /**
     * Return an array of standard cards in one suit.
     *
     * @param string $suit Standard suit name
     * @return array         Array of Card objects
     */
    public static function suit(string $suit): array
    {
        $suit = rtrim($suit, 's');

        if (isset(self::$standard_faces[$suit])) {
            return self::$standard_faces[$suit];
        } else {
            throw new \InvalidArgumentException('Invalid suit');
        }
    }


    /**
     * Return an array of suit letters.
     * @return string[]
     */
    public static function suitLetters(): array
    {
        return array_keys(self::$suits);
    }


    /**
     * Return the standard values as letters.
     *
     * @return array
     */
    public static function valueLetters(): array
    {
        return array_map(function ($v) {
            return rtrim($v, 'c');
        },
            self::suit('clubs')
        );
    }


    /**
     * Count the number of standard cards available.
     *
     * @return int
     */
    public static function standardCardsCount(): int
    {
        $count = 0;

        foreach (self::$standard_faces as $faces) {
            $count += count($faces);
        }

        return $count;
    }



    //////////////////////////////////////////////
    // Shuffling
    // All shuffling methods must work on any
    // number of cards in the deck
    //////////////////////////////////////////////


    /**
     * Completely random shuffle
     *
     * @return void
     * @throws \Exception
     */
    public function shuffleRandom(): void
    {
        $this->container = Deck::randomizeArray($this->container);
    }


    /**
     * Execute a perfect faro shuffle. Set $type to true
     * for an in faro.
     *
     * @param int $cut_at
     * @param bool $in True = In Faro
     * @return void
     */
    public function shuffleFaro(int $cut_at, bool $in = true): void
    {
        if ($cut_at < 1 or $cut_at >= $this->count()) {
            return;
        }

        $cut = $this->cutAt($cut_at, false);

        $result = [];

        // In Faro, top card becomes second card
        if ($in) {
            $result[] = array_shift($cut['bottom']);
        }

        while (count($cut['top']) > 0 or count($cut['bottom']) > 0) {
            $top_card = array_shift($cut['top']);
            $bottom_card = array_shift($cut['bottom']);

            if ($top_card) {
                $result[] = $top_card;
            }

            if ($bottom_card) {
                $result[] = $bottom_card;
            }
        }

        $this->container = $result;
    }


    /**
     * Anti Faro shuffle, exact opposite of Faro. 'In' means top card
     * goes into deck, 'out' means top card stays on outside (top of deck)
     * @param int $limit
     * @param bool $in
     * @return void
     */
    public function shuffleAntiFaro(int $limit, bool $in = true): void
    {
        if ($limit < 1 )
        {
            return;
        }

        $extracted = [];

        // Find the remainder of the cards
        $cut = $this->cutAt( ($limit*2), false);

        // Remove every other card
        for( $index = 0; $index < ($limit*2); $index+=2 )
        {
            // Stop when card doesn't exist to extract
            if( !isset($cut['top'][$index]) )
            {
                break;
            }

            $extracted[] = $cut['top'][$index];

            unset($cut['top'][$index]);
        }

        if( $in )
        {
            $this->container = array_merge( $cut['top'], $extracted, $cut['bottom']);
        }
        else
        {
            $this->container = array_merge( $extracted, $cut['top'], $cut['bottom'] );
        }
    }


    /**
     * Execute an out Antifaro at halfway
     * @return void
     */
    public function shuffleOutAntiFaro(): void
    {
        $half = $this->getPercent(50 );

        $this->shuffleAntiFaro($half, false);
    }


    /**
     * Execute an in Antifaro at halfway
     * @return void
     */
    public function shuffleInAntiFaro(): void
    {
        $half = $this->getPercent(50);

        $this->shuffleAntiFaro($half);
    }


    /**
     * Execute an out faro at halfway
     * @return void
     */
    public function shuffleOutFaro(): void
    {
        $half = $this->getPercent(50);

        $this->shuffleFaro($half, false);
    }


    /**
     * Execute an in faro at halfway
     * @return void
     */
    public function shuffleInFaro(): void
    {
        $half = $this->getPercent(50);

        $this->shuffleFaro($half);
    }


    /**
     * The top third to 2 thirds is riffle shuffled
     * into the bottom packet.
     *
     * The max_split_deviation defines the number of card either
     * side of half that the split can be
     *
     * @param int $min Min size of each riffled block
     * @param int $max Max size of each riffled block
     * @param int $max_split_deviation Max number of cards that the split can deviate from half
     * @return void
     * @throws \Exception
     */
    public function shuffleRiffle(int $min = 1, int $max = 4, int $max_split_deviation = 3): void
    {
        $result = [];

        if (empty($max_split_deviation)) {
            // Split in half
            $n = $this->getPercent(50);
        } else {
            // Generate the number of cards to deviate from half
            $deviation = random_int(($max_split_deviation * -1), $max_split_deviation);

            $n = $this->getPercent(50) - $deviation;
        }

        // Cut the deck into 2 portions
        $cut = $this->cutAt($n, false);

        // Keep going while there are cards left
        while (count($result) < $this->count()) {
            // Packet size
            $n1 = random_int($min, $max);
            $n2 = random_int($min, $max);

            // Take some cards from the bottom
            $result = array_merge(
                $result,
                array_splice($cut['bottom'], 0, $n1, []),
                array_splice($cut['top'], 0, $n2, [])
            );
        }

        $this->container = $result;
    }


    /**
     * Overhand shuffle where each block pulled off the stock
     * is between 1 and $num cards. $num can be a percentage
     * of the remaining stock (and is by default)
     *
     * @param int $num Max number of cards/ percentage in a block
     * @param int $min Min number of cards in a block
     * @param bool $percent Whether the num is a percentage.
     * @param int $throw_threshold A number of cards below which the stock is thrown
     * @return void
     * @throws \Exception
     */
    public function shuffleOverhand(int $num = 40, int $min = 1, bool $percent = true, int $throw_threshold = 13): void
    {
        $result = [];

        while ($this->count() > 0) {
            // Generate the max size of the block
            if ($percent) {
                // Max is a percentage of the stock
                $max = $this->getPercent($num);
            } else {
                // Max is a number of cards
                $max = $num;
            }

            // Check max
            if ($max < 1) {
                $max = 1;
            } elseif ($max > $this->count()) {
                $max = $this->count();
            }

            // Determine if stock should be thrown
            if (!empty($throw_threshold) and $this->count() < $throw_threshold) {
                $n = $this->count();
            } else {
                // Generate random number of cards for block
                $n = random_int($min, $max);
            }

            $result = array_merge(array_slice($this->container, 0, $n), $result);

            $this->container = array_slice($this->container, $n);
        }

        $this->container = $result;
    }


    /**
     * A fine overhand shuffle where the maximum number
     * of cards in a block is 6 and the minimum is 2.
     *
     * @return void
     * @throws \Exception
     */
    public function shuffleOverhandFine(): void
    {
        $this->shuffleOverhand(6, 2, false, 6);
    }






    //////////////////////////////////////////////
    // Cutting
    // Work on any number of cards in the deck
    //////////////////////////////////////////////


    /**
     * Cut the cards at a specific number.
     * If re-assemble is false, a copy of the deck is returned
     * in an array with arr['top'] the top half and arr['bottom'] the bottom
     *
     * @param int $n
     * @param bool $reassemble Whether the deck is reassembled
     * @return array|null
     */
    public function cutAt(int $n, bool $reassemble = true): null|array
    {
        $bottom = array_slice($this->container, $n);

        $top = array_slice($this->container, 0, $n);

        // Don't reassemble, return two blocks of cards
        if (!$reassemble) {
            return
                [
                    'top' => $top,
                    'bottom' => $bottom
                ];
        }

        $this->container = array_merge($bottom, $top);

        return null;
    }


    /**
     * A random cut anywhere in the deck
     *
     * @param int $min
     * @param null $max
     * @param bool $reassemble
     * @return int
     * @throws \Exception
     */
    public function cutRandom(int $min = 0, $max = null, bool $reassemble = true): int
    {
        if (is_null($max)) {
            $max = $this->count() - 1;
        }

        if ($min < 0) {
            $min = 0;
        }

        if ($min > $this->count() - 1) {
            $min = $this->count() - 1;
        }

        if ($max < 1) {
            $max = 1;
        }

        if ($max > $this->count() - 1) {
            $max = $this->count() - 1;
        }

        $n = random_int($min, $max);

        $this->cutAt($n, $reassemble);

        return $n;
    }


    /**
     * Cut halfway through the deck.
     *
     * @return void
     */
    public function cutHalf(): void
    {
        $n = floor($this->count() / 2);

        $this->cutAt($n);
    }


    /**
     * Cut within middle third of deck.
     *
     * @return int
     * @throws \Exception
     */
    public function cutMiddleThird():int
    {
        $n = floor($this->count() / 2);

        return $this->cutRandom($this->getPercent(33.333) + 1, $this->count() - $this->getPercent(33.333) - 1);
    }





    //////////////////////////////////////////////
    // Dealing
    // Work on any number of cards in the deck
    //////////////////////////////////////////////



    /**
     * Deal cards off the deck. Returns the result as an array of hands
     * and stock, all groups of cards in Deck instances or in a reassembled
     * deck.
     *
     * @param int $hand_count Number of hands
     * @param int|null $limit Max number of cards in each hand.
     * @param bool $reassemble Whether to reassemble into the this deck instance
     * @param bool $stock_position False for stock on bottom of deck, true for top
     * @return array|array[]|null
     */
    public function deal(
        int $hand_count,
        int $limit = null,
        bool $reassemble = true,
        bool $stock_position = false
    ): ?array
    {
        $result = ['hands' => [] ];

        // Calculate limit if not provided
        if (is_null($limit)) {
            $limit = ceil($this->count() / $hand_count);
        }

        // Cut off the exact number of cards
        $cut = $this->cutAt( $hand_count * $limit, false);

        for ($hand = 0; $hand < $hand_count; $hand++)
        {
            $deck = new Deck;

            for ($c = ($limit-1); $c >= 0; $c--)
            {
                $index = ($c * $hand_count) + $hand;

                if (isset($cut['top'][$index]))
                {
                    $deck->append( [$cut['top'][$index]] );
                }
            }

            $result['hands'][$hand] = $deck;
        }

        if( $reassemble )
        {
            $this->clear();

            // Add stock to top of deck
            if( $stock_position === true )
            {
                $this->append( $cut['bottom'] );
            }

            // Loop through hands in reverse order, adding to result
            // from top to bottom
            for ($hand = ($hand_count-1); $hand >= 0; $hand--)
            {
                $this->append( $result['hands'][$hand] );
            }

            // Add stock to bottom of deck
            if( $stock_position === false )
            {
                $this->append( $cut['bottom'] );
            }
        }
        else
        {
            $result['stock'] = new Deck( $cut['bottom'] );

            return $result;
        }

        return null;
    }


    /**
     * Reverse the order of the deck.
     *
     * @return void
     */
    public function reverseOrder(): void
    {
        $this->container = array_reverse( $this->container );
    }

}
