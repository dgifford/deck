<?php

Namespace dgifford\Deck;


class Card implements \JsonSerializable
{
    /**
     * Unique ID given to each card
     * @var int|null
     */
    protected ?int $id = null;


    /**
     * Code representing the front of the card.
     * Case insensitive.
     * @var string
     */
    protected string $front;


    /**
     * Code representing the back of the card.
     * Case insensitive.
     * @var string
     */
    protected string $back = 'bb';


    /**
     * Type of card as defined in Deck::standard_faces
     * @var string
     */
    protected string $type = 'unknown';


    /**
     * The vertical orientation of the card. This only
     * effects cards without a horizontal line of
     * symmetry, e.g 7s, 9s, court cards, miss-indexed.
     * False is the standard revolved, top-heavy.
     * @var bool
     */
    protected bool $revolved = false;


    /**
     * Whether a card is reversed.
     * @var bool
     */
    protected bool $reversed = false;


    /**
     * Whether the card is selected.
     * @var bool
     */
    protected bool $selected = false;


    /**
     * Codes that represent options set on the card
     * @var string[]
     */
    protected array $option_codes =
        [
            'o' => 'revolved',
            'r' => 'reversed',
            's' => 'selected',
        ];


    /**
     * Separator used in combined card faces, e.g.
     * bb:ah
     * @var string
     */
    protected string $separator = ':';


    /**
     * Constructor calls the set method with any arguments.
     */
    public function __construct(...$args)
    {
        $this->set(...$args);
    }


    /**
     * Properties returned when serialized as JSON
     * @return array
     */
    public function jsonSerialize(): array
    {
        return
            [
                'id' => $this->getID(),
                'value' => $this->getValue(),
                'suit' => $this->getSuitLetter(),
                'front' => $this->getFront(),
                'back' => $this->getBack(),
                'revolved' => $this->getRevolved(),
                'reversed' => $this->getReversed(),
                'type' => $this->getType(),
                'selected' => $this->getSelected(),
            ];
    }




    //////////////////////////////////////////////
    // Boolean tests
    //////////////////////////////////////////////


    /**
     * Returns true if the front of the card has a valid suit letter.
     *
     * @return bool
     */
    public function hasSuit(): bool
    {
        if (empty($this->getSuitLetter())) {
            return false;
        }

        return true;
    }





    //////////////////////////////////////////////
    // Setters
    //////////////////////////////////////////////


    /**
     * Sets the properties of the card. Accepts a
     * front string, a combined front/back string,
     * separate front and back strings or an array.
     *
     * @param string|array $front
     * @param string $back
     */
    public function set(string|array $front, string $back = 'bb'): void
    {
        // String provided
        if (is_string($front)) {
            $front = trim($front);
            $back = trim($back);

            if (empty($front)) {
                throw new \InvalidArgumentException('Invalid card');
            }

            $split = explode($this->separator, $front);

            if (count($split) > 1) {
                $front = trim($split[0]);
                $back = trim($split[1]);

                if (isset($split[2])) {
                    $options = str_split(trim($split[2]));

                    foreach ($options as $option) {
                        if (isset($this->option_codes[$option])) {
                            $this->{'set' . ucfirst($this->option_codes[$option])}(true);
                        }
                    }
                }
            }

            $this->setFront($front);

            if (!empty($back)) {
                $this->setBack($back);
            }
        } elseif (is_array($front) and !empty($front)) {
            foreach ($front as $key => $value) {
                if (is_string($key) and is_callable([$this, 'set' . ucfirst($key)])) {
                    $method = 'set' . ucfirst($key);

                    $this->$method($value);
                }
            }
        } else {
            throw new \InvalidArgumentException('Invalid card');
        }

        $this->setType();
    }


    /**
     * Set the ID of a card.
     *
     * @param int $id
     */
    public function setID(int $id): void
    {
        $this->id = $id;
    }


    /**
     * Set the card front.
     *
     * @param string $front
     */
    public function setFront(string $front): void
    {
        $this->front = strtolower($front);
    }


    /**
     * Set the card back.
     *
     * @param string $back
     */
    public function setBack(string $back): void
    {
        $this->back = strtolower($back);
    }


    /**
     * Set the card type. Must be included in the
     * self::types array.
     *
     * @param string|null $type
     */
    public function setType(string $type = null): void
    {
        if (is_null($type)) {
            foreach (Deck::$standard_faces as $type => $faces) {
                if (in_array($this->getFront(), $faces)) {
                    $this->type = $type;
                    return;
                }
            }

            $this->type = 'unknown';
        } else {
            $this->type = $type;
        }
    }


    /**
     * Set the card revolved, true or false.
     * Default is false, which means heaviset end is
     * top, e.g. odd end of a 7.
     *
     * @param bool $revolved
     */
    public function setRevolved(bool $revolved): void
    {
        $this->revolved = $revolved;
    }


    /**
     * Toggle the card revolved
     **/
    public function toggleRevolved(): void
    {
        $this->revolved = !$this->revolved;
    }


    /**
     * Set the card as reversed, true or false.
     * False means face up for a standard card
     *
     * @param bool $reversed
     */
    public function setReversed(bool $reversed): void
    {
        $this->reversed = $reversed;
    }


    /**
     * Toggle the card reversad
     *
     */
    public function toggleReversed(): void
    {
        $this->reversed = !$this->reversed;
    }


    /**
     * Set the selected attribute
     *
     * @param boolean $selected
     */
    public function setSelected(bool $selected): void
    {
        $this->selected = $selected;
    }


    /**
     * Toggle the selected attribute
     *
     */
    public function toggleSelected():void
    {
        $this->selected = !$this->selected;
    }






    //////////////////////////////////////////////
    // Getters
    //////////////////////////////////////////////


    /**
     * Get the card ID
     * @return int|null
     */
    public function getID(): ?int
    {
        return $this->id;
    }


    /**
     * Get the selected status of the card
     * TODO: Is this the responsibility of the card?
     * @return bool
     */
    public function getSelected(): bool
    {
        return $this->selected;
    }


    /**
     * Get the front of the card
     * @return string
     */
    public function getFront(): string
    {
        return $this->front;
    }


    /**
     * Synonym for the front of the card
     * @return string
     */
    public function getFace(): string
    {
        return $this->getFront();
    }


    /**
     * Get the back of the card
     * @return string
     */
    public function getBack(): string
    {
        return $this->back;
    }


    /**
     * Get the revolved status
     * @return bool
     */
    public function getRevolved(): bool
    {
        return $this->revolved;
    }


    /**
     * Get the reversed status of the card
     * @return bool
     */
    public function getReversed(): bool
    {
        return $this->reversed;
    }


    /**
     * Get the type of the card
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }


    /**
     * Get the suit letter from the front of the card.
     * Returns an empty string if none found.
     * @return string
     */
    public function getSuitLetter(): string
    {
        if (Deck::isSuitedFace($this->getFront())) {
            return substr($this->getFront(), -1);
        }

        return '';
    }


    /**
     * Get value of the card from the front
     * @return false|string
     */
    public function getValue(): string
    {
        if ($this->hasSuit()) {
            return substr($this->front, 0, strlen($this->front) - 1);
        } else {
            return '';
        }
    }


    /**
     * Return a text code for the card.
     *
     * @return string
     */
    public function getCode(): string
    {
        $code =
            [
                $this->getFront(),
                $this->getBack(),
                ($this->getSelected() ? 's' : '') .
                ($this->getReversed() ? 'r' : '') .
                ($this->getRevolved() ? 'o' : ''),
            ];

        return rtrim(
            implode($this->separator, $code)
            , $this->separator
        );
    }


    /**
     * Get the front of the card.
     * @return string
     */
    public function front(): string
    {
        return $this->getFront();
    }


    /**
     * Get the back of the card
     * @return string
     */
    public function back(): string
    {
        return $this->getBack();
    }
}
