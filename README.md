# Deck

Simulates a deck of playing cards as close as possible to a 'real' deck.

A deck can include any combination of cards from Bicycle New Deck Order to an alphabet deck, and mixed using real-world shuffles such as overhand, riffle or faro.

Cards can be reversed or rotated, just as in real life. 

## Randomness

The native PHP function `random_int()` has been used to generate randomness. This provides randomness which (according to the PHP manual) is suitable for use where unbiased results are critical.

## Shuffling

The shuffling and cutting methods provided attempt to simulate human actions. With cuts for example, a truly random cut has an equal chance of occurring at any position in a deck, however a real person is most likely to cut within the middle third of the deck, so a 'Middle third' cut is provided.

A person performing an overhand shuffle can pull of chunks of cards varying greatly in size and will tend to stop well before the end of the card stock. The overhand shuffle method allows you to adjust for this providing arguments for the minimum and maximum size of blocks, and a threshold for throwing the remaining stock.

## Deck/ Card Data Structure

Cards in the deck are identified by an incremental index where 0 is the 'top' of the deck. This index is reset by default after all actions.

Cards have the following properties:

- `id` A unique integer ID which is immutable, so duplicate cards can be tracked.
- `front` A string representing the front of the card.
- `back` A string representing the back of the card.
- `selected` If the card is selected in the deck.
- `reversed` If the card is reversed, e.g. 'face down' in the deck.
- `revolved` If the card is revolved, e.g. a non-symmetrical card like a court card has been rotated

Cards can have any string representing their front or back, however strings like `ah` will be recognised as standard types, e.g. `ah` is a `heart`.


## To do:

- Add card types

	- ESP symbols

	- Miss-indexed

- Add anti faro shuffle

- Add chop shuffle

- Add deals

	- Into piles

	- Down/ under and Under/ down